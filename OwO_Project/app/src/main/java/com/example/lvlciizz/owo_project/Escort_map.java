package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Escort_map extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escort_map);

        Button doradoBt = (Button) findViewById(R.id.button_escort_dorado);
        Button junkertownBt = (Button) findViewById(R.id.button_escort_junkertown);
        Button route66Bt = (Button) findViewById(R.id.button_escort_route66);
        Button watchpointBt = (Button) findViewById(R.id.button_escort_watchpointgibraltar);

        doradoBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_doradoBt = new Intent(Escort_map.this, side.class);
                startActivity(intent_doradoBt);
            }
        });

        junkertownBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_junkertownBt = new Intent(Escort_map.this, side.class);
                startActivity(intent_junkertownBt);
            }
        });

        route66Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_route66Bt = new Intent(Escort_map.this, side.class);
                startActivity(intent_route66Bt);
            }
        });

        watchpointBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_watchpointBt = new Intent(Escort_map.this, side.class);
                startActivity(intent_watchpointBt);
            }
        });
    }
}
