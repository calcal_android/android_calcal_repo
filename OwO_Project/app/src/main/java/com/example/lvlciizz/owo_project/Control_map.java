package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Control_map extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_map);

        Button iliosBt = (Button) findViewById(R.id.button_control_ilios);
        Button lijingBt = (Button) findViewById(R.id.button_control_lijing_tower);
        Button nepalBt = (Button) findViewById(R.id.button_control_nepal);
        Button oasisBt = (Button) findViewById(R.id.button_control_oasis);

        iliosBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_iliosBt = new Intent(Control_map.this, select_point.class);
                startActivity(intent_iliosBt);
            }
        });

        lijingBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_lijingBt = new Intent(Control_map.this, select_point.class);
                startActivity(intent_lijingBt);
            }
        });

        nepalBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_nepalBt = new Intent(Control_map.this, select_point.class);
                startActivity(intent_nepalBt);
            }
        });

        oasisBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_oasisBt = new Intent(Control_map.this, select_point.class);
                startActivity(intent_oasisBt);
            }
        });
    }
}
