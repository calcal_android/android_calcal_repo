package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Heroes extends AppCompatActivity {

    private FloatingActionButton homeBt;

    GridLayout mainGrid;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heroes);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        homeBt = findViewById(R.id.homeBt);
        homeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(Heroes.this, HOME.class);
                startActivity(home_intent);
            }
        });

        //Set Event
        setSingleEvent(mainGrid);
    }

    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
       /* for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI == 0) {
                        Intent intent = new Intent(Heroes.this, Hero_ana.class);
                        startActivity(intent);
                    }
                    else if (finalI == 1){
                        Intent intent = new Intent(Heroes.this, Hero_bastion.class);
                        startActivity(intent);
                    }
                    else if (finalI == 2){
                        Intent intent = new Intent(Heroes.this, Hero_dva.class);
                        startActivity(intent);
                    }
                    else if (finalI == 3){
                        Intent intent = new Intent(Heroes.this, Hero_genji.class);
                        startActivity(intent);
                    }
                    else if (finalI == 4){
                        Intent intent = new Intent(Heroes.this, Hero_hanzo.class);
                        startActivity(intent);
                    }
                    else if (finalI == 5){
                        Intent intent = new Intent(Heroes.this, Hero_junkrat.class);
                        startActivity(intent);
                    }
                    else if (finalI == 6){
                        Intent intent = new Intent(Heroes.this, Hero_lucio.class);
                        startActivity(intent);
                    }
                    else if (finalI == 7){
                        Intent intent = new Intent(Heroes.this, Hero_mccree.class);
                        startActivity(intent);
                    }
                    else if (finalI == 8){
                        Intent intent = new Intent(Heroes.this, Hero_mei.class);
                        startActivity(intent);
                    }
                    else if (finalI == 9){
                        Intent intent = new Intent(Heroes.this, Hero_mercy.class);
                        startActivity(intent);
                    }
                    else if (finalI == 10){
                        Intent intent = new Intent(Heroes.this, Hero_pharah.class);
                        startActivity(intent);
                    }
                    else if (finalI == 11){
                        Intent intent = new Intent(Heroes.this, Hero_reaper.class);
                        startActivity(intent);
                    }
                    else if (finalI == 12){
                        Intent intent = new Intent(Heroes.this, Hero_reinhardt.class);
                        startActivity(intent);
                    }
                    else if (finalI == 13){
                        Intent intent = new Intent(Heroes.this, Hero_roadhog.class);
                        startActivity(intent);
                    }
                    else if (finalI == 14){
                        Intent intent = new Intent(Heroes.this, Hero_soldier76.class);
                        startActivity(intent);
                    }
                    else if (finalI == 15){
                        Intent intent = new Intent(Heroes.this, Hero_sombra.class);
                        startActivity(intent);
                    }
                    else if (finalI == 16){
                        Intent intent = new Intent(Heroes.this, Hero_symmetra.class);
                        startActivity(intent);
                    }
                    else if (finalI == 17){
                        Intent intent = new Intent(Heroes.this, Hero_tracer.class);
                        startActivity(intent);
                    }
                    else if (finalI == 18){
                        Intent intent = new Intent(Heroes.this, Hero_torbjorn.class);
                        startActivity(intent);
                    }
                    else if (finalI == 19){
                        Intent intent = new Intent(Heroes.this, Hero_widowmaker.class);
                        startActivity(intent);
                    }
                    else if (finalI == 20){
                        Intent intent = new Intent(Heroes.this, Hero_winston.class);
                        startActivity(intent);
                    }
                    else if (finalI == 21){
                        Intent intent = new Intent(Heroes.this, Hero_zarya.class);
                        startActivity(intent);
                    }
                    else if (finalI == 22){
                        Intent intent = new Intent(Heroes.this, Hero_zenyatta.class);
                        startActivity(intent);
                    }


                }
            });
        }*/

        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(Heroes.this,Hero_Details.class);
                    String hero_position = Integer.toString(finalI);
                    intent.putExtra("info",hero_position);
                    startActivity(intent);

                }
            });
        }
    }
}
