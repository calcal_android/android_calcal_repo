package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class herosbuildteam extends AppCompatActivity {

    GridLayout mainGrid;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heroes);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        //Set Event
        setSingleEvent(mainGrid);
    }

    private void setSingleEvent(GridLayout mainGrid) {




        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent = new Intent(herosbuildteam.this,Createbuild.class);
                    String h_pst = Integer.toString(finalI);
                    Intent iin= getIntent();
                    Bundle b = iin.getExtras();

                    String item_selected = (String) b.get("slc");

                    intent.putExtra("hero",h_pst);
                    intent.putExtra("slc",item_selected);
                    startActivity(intent);

                }
            });
        }
    }
}
