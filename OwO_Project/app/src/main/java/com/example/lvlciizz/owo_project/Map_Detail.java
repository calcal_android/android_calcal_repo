package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Map_Detail extends AppCompatActivity {

    private FloatingActionButton backBt;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map__detail);

        TextView Map_Header = findViewById(R.id.textView_mapdetail);
        ImageView Map_title = findViewById(R.id.map_title_pic);
        ImageView Map_detail = findViewById(R.id.map_detail_pic);
        TextView Map_infor = findViewById(R.id.detail_map);

        backBt = findViewById(R.id.backBt);

        Map_Header.setText("      MAP DETAILS");

        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getIntent() != null) {
            String info = getIntent().getStringExtra("info");
            if (info.equals("0")) {
                Map_title.setImageResource(R.drawable.map_title_dorado);
                Map_detail.setImageResource(R.drawable.map_detail_dorado_01);
                Map_infor.setText("        It is Festival de la Luz in Dorado, an annual celebration of the end of the Omnic Crisis and the period of darkness – both figurative and literal – that engulfed Mexico when the omnics destroyed much of the country’s power grid and infrastructure. But there is a new dawn, as LumériCo and its CEO, war hero and former president Guillermo Portero, are unveiling a string of fusion plants that promise to deliver clean, free energy to the Mexican people.\n" +
                        "\n" +
                        "The attacking team begins in Missión Dorado, a historic location famous for the sound of the ornamental bells that hang amidst its arches.");
            } else if (info.equals("1")) {
                Map_title.setImageResource(R.drawable.map_title_eichenwalde);
                Map_detail.setImageResource(R.drawable.map_detail_eichenwalde);
                Map_infor.setText("        The town was the site of one of the most famous battles during the Omnic Crisis. It was here that the leader of the Crusaders, Balderich von Adler, and a handful of his best soldiers, including his pupil, Reinhardt Wilhelm, made a last stand against an advancing automaton army.[2] Since Eichenwalde was in the way of a larger omnic advance to Stuttgart, the town was evacuated on October 11, from 3 to 8 o'clock.[3] Outnumbered and outgunned, almost all Crusaders were slain during the resulting combat, including the organizations leader. However, thanks to their valiant efforts, the German military was able to push back the omnic offensive and win the fight.[4][3] In the present day, Eichenwalde lies abandoned, and the forest has slowly begun overtaking the village. However, the scars of war will never completely fade.");
            } else if (info.equals("2")) {
                Map_title.setImageResource(R.drawable.map_title_hanamura);
                Map_detail.setImageResource(R.drawable.map_detail_hanamura);
                Map_infor.setText("        Hanamura is a well-preserved village of unassuming shops and quiet streets, known mostly for its idyllic cherry blossom festival every spring. But to those who know its history, Hanamura is the ancestral home of the Shimada ninja clan, which had grown over the centuries to become a powerful criminal organization. As its neighboring cities expanded, Hanamura was encircled, and eventually it was incorporated as a district within a larger city. For now, the imposing compound of the Shimada family lies empty, but that peace may soon be broken.");
            } else if (info.equals("3")) {
                Map_title.setImageResource(R.drawable.map_title_hollywood);
                Map_detail.setImageResource(R.drawable.map_detail_hollywood);
                Map_infor.setText("        Welcome to the glitz and glamour of Hollywood, California, where palm trees and fancy cars line the streets, and movie stars, directors, and high-powered studio executives rub shoulders for a chat and a drink at Galand's. Down the street from the Mandarin Theatre, Goldshire Studios' omnic film auteur, HAL-Fred Glitchbot, has created his two latest films, They Come from Beyond the Moon and Six-Gun Killer, to varying amounts of critical and commercial acclaim. However, even Tinseltown has been gripped by anti-omnic sentiment, and the outspoken director has become a prime target in the escalating conflict.");
            } else if (info.equals("4")) {
                Map_title.setImageResource(R.drawable.map_title_horizon);
                Map_detail.setImageResource(R.drawable.map_detail_horizon_lunar);
                Map_infor.setText("        Built as a first step towards humanity's renewed exploration of space, the Horizon Lunar Colony’s goal was to examine the effects of prolonged extraterrestrial habitation—on human and ape alike. The scientists' research proved incredibly promising...until, suddenly, all contact and communications with the base were lost.");
            } else if (info.equals("5")) {
                Map_title.setImageResource(R.drawable.map_title_ilios);
                Map_detail.setImageResource(R.drawable.map_detail_ilios);
                Map_infor.setText("        Situated atop a small island rising from the Aegean Sea, Ilios is a postcard-perfect Mediterranean town, with a bustling harborside, winding paths for rambling hillside strolls, and gorgeous vistas. It is the ideal vacation stop for people looking for a place to relax, or for those interested in exploring the ruins scattered at the top of the island, where many artifacts and relics of the ancient world have been recently unearthed. The ruins were declared an internationally protected heritage site. However, Talon attempted to steal the artifacts.");
            } else if (info.equals("6")) {
                Map_title.setImageResource(R.drawable.map_title_junkertown);
                Map_detail.setImageResource(R.drawable.map_detail_junkertown);
                Map_infor.setText("        Junkertown is an Escort map located in the harsh and unforgiving Australian Outback. Constructed from the remains of a destroyed omnium, it's now the home to a band of lawless scavengers known as the Junkers, led by their cutthroat Queen. When they aren’t pillaging the omnium's skeleton for anything of value, the Junkers blow off steam in the Scrapyard—a massive gladiatorial arena whose combatants fight for glory, riches...and to survive.");
            } else if (info.equals("7")) {
                Map_title.setImageResource(R.drawable.map_title_kings_row);
                Map_detail.setImageResource(R.drawable.map_detail_kings_row);
                Map_infor.setText("        King's Row is an upscale, cosmopolitan neighborhood of London, England, but just beneath its peaceful surface, tensions between omnics and humans are running high. While much of modern Britain was built on the backs of omnic laborers, they have been denied the basic rights that humans have, with most omnics forced to live in the dense, claustrophobic city-beneath-the-city known by some as \"the Underworld.\" Of greater concern is that recent demonstrations by pro-omnic-rights protestors have resulted in violent clashes with the police, and a solution is nowhere in sight.");
            } else if (info.equals("8")) {
                Map_title.setImageResource(R.drawable.map_title_lijiang_tower);
                Map_detail.setImageResource(R.drawable.map_detail_lijiang_tower_01);
                Map_infor.setText("        Lijiang Tower was built in the heart of a modern Chinese metropolis, its busy streets lined with stores, gardens, restaurants, and famous night markets, where foods from around the region are available at all hours. The tower itself is home to one of the leading companies in China's state-of-the-art space industry, Lucheng Interstellar, an organization with a long pioneering history that is currently pushing the boundaries of space exploration.");
            } else if (info.equals("9")) {
                Map_title.setImageResource(R.drawable.map_title_napal);
                Map_detail.setImageResource(R.drawable.map_detail_napal_01);
                Map_infor.setText("        Years ago, a group of omnic robots experienced what they described as a spiritual awakening. They abandoned their programmed lives to establish a monastery high in the Himalayas, where like-minded omnics could gather to meditate on the nature of their existence. Led by their spiritual leader, Tekhartha Mondatta, they took over the ruins of an ancient monastery and turned it into the home of the Shambali, a place where omnics and humans alike make pilgrimages in the hopes of finding a greater truth.");
            } else if (info.equals("10")) {
                Map_title.setImageResource(R.drawable.map_title_numbani);
                Map_detail.setImageResource(R.drawable.map_detail_numbani_01);
                Map_infor.setText("        Known as the \"City of Harmony,\" Numbani is one of the few places where omnics and humans live as equals. This collaboration has led to the creation of one of the world's greatest and most technologically advanced cities in the short time since its establishment after the end of the Omnic Crisis. As part of this year's Unity Day festivities celebrating the city's founding, the gauntlet of the infamous Doomfist is being exhibited at the Numbani Heritage Museum.");
            } else if (info.equals("11")) {
                Map_title.setImageResource(R.drawable.map_title_oasis);
                Map_detail.setImageResource(R.drawable.map_detail_oasis);
                Map_infor.setText("        Oasis is one of the world's most advanced cities, a shining jewel rising from the Arabian Desert. A monument to human ingenuity and invention, researchers and academics from around the region came together to found a city dedicated to scientific progress without restraints. The city and its inhabitants are governed by the Ministries, a collection of brilliant minds who possess many secrets that have attracted the interest of powerful organizations from around the world.");
            } else if (info.equals("12")) {
                Map_title.setImageResource(R.drawable.map_title_route66);
                Map_detail.setImageResource(R.drawable.map_detail_route66);
                Map_infor.setText("        Though the travelers and road trippers who used to cross the US on historic Route 66 are gone, the Main Street of America still stands, a testament to a simpler time. The gas stations, roadside shops, and cafes have gone into disuse, and the fabled Deadlock Gorge is mostly seen from the comfort of transcontinental train cars. But amid the fading monuments of that earlier era, the outlaws of the Deadlock Gang are planning their biggest heist yet.");
            } else if (info.equals("13")) {
                Map_title.setImageResource(R.drawable.map_title_temple_of_anubis);
                Map_detail.setImageResource(R.drawable.map_detail_temple_of_anubis);
                Map_infor.setText("        Nestled among the ancient ruins of the Giza Plateau on the outskirts of Cairo, the Temple of Anubis is one of many new excavations in the area. While most believe that the site is of interest for archaelogical reasons, it also hides the entrance to a research facility that extends deep beneath the earth. The facility is alleged to house an artificial intelligence, but the whole truth is known only to a select few. Not even the agents of Helix Security International who guard the facility know what they're protecting.");
            } else if (info.equals("14")) {
                Map_title.setImageResource(R.drawable.map_title_volskaya);
                Map_detail.setImageResource(R.drawable.map_detail_volskaya_01);
                Map_infor.setText("        Russia was one of the countries hit hardest by the Omnic Crisis, but during the rebuilding process, it rode the wave of the mechanized labor industry's revitalization and entered a period of rapid growth. However, after recent attacks from the long-dormant Siberian omnium, Russia has returned to war footing, guarding its sparkling cities with the massive, human-piloted Svyatogor mechs developed and produced by Volskaya Industries.");
            } else if (info.equals("15")) {
                Map_title.setImageResource(R.drawable.map_title_watchpoint_gibratar);
                Map_detail.setImageResource(R.drawable.map_detail_watchpoint_gibratar_01);
                Map_infor.setText("        At its height, Overwatch maintained a number of bases around the world, each with its own purpose: peacekeeping, scientific research, or in the case of Watchpoint: Gibraltar, providing an orbital launch facility. The base was mothballed along with the rest of Overwatch's installations, but there have been recent reports of activity within the perimeter. Could this indicate the presence of former Overwatch agents, or is this the work of organizations with more nefarious intentions?");
            }
        }
    }
}
