package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Createbuild extends AppCompatActivity {

//    int item_selected;

    private FloatingActionButton homeBt;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createbuild);

        ImageButton mapImg = findViewById(R.id.mapImg);
        ImageButton Btn1 = findViewById(R.id.hero1);
        ImageButton Btn2 = findViewById(R.id.hero2);
        ImageButton Btn3 = findViewById(R.id.hero3);
        ImageButton Btn4 = findViewById(R.id.hero4);
        ImageButton Btn5 = findViewById(R.id.hero5);
        ImageButton Btn6 = findViewById(R.id.hero6);

        homeBt = findViewById(R.id.homeBt);
        homeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(Createbuild.this, HOME.class);
                startActivity(home_intent);
            }
        });

        Intent intent = getIntent();
        final String mapno = intent.getStringExtra("mapno");
        final String hero1no = intent.getStringExtra("hero1no");
        final String hero2no = intent.getStringExtra("hero2no");
        final String hero3no = intent.getStringExtra("hero3no");
        final String hero4no = intent.getStringExtra("hero4no");
        final String hero5no = intent.getStringExtra("hero5no");
        final String hero6no = intent.getStringExtra("hero6no");

        if(getIntent() != null) {
            // CHECK MAPS
            if(mapno.equals("0")){
                mapImg.setImageResource(R.drawable.map_dorado);
            }else if(mapno.equals("1")){
                mapImg.setImageResource(R.drawable.map_elchenwalde);
            }else if(mapno.equals("2")){
                mapImg.setImageResource(R.drawable.map_hanamura);
            }else if(mapno.equals("3")){
                mapImg.setImageResource(R.drawable.map_hollywood);
            }else if(mapno.equals("4")){
                mapImg.setImageResource(R.drawable.map_horizon);
            }else if(mapno.equals("5")){
                mapImg.setImageResource(R.drawable.map_ilios);
            }else if(mapno.equals("6")){
                mapImg.setImageResource(R.drawable.map_junkertown);
            }else if(mapno.equals("7")){
                mapImg.setImageResource(R.drawable.map_kingsrow);
            }else if(mapno.equals("8")){
                mapImg.setImageResource(R.drawable.map_lijiang_tower);
            }else if(mapno.equals("9")){
                mapImg.setImageResource(R.drawable.map_nepal);
            }else if(mapno.equals("10")){
                mapImg.setImageResource(R.drawable.map_numbani);
            }else if(mapno.equals("11")){
                mapImg.setImageResource(R.drawable.map_oasis);
            }else if(mapno.equals("12")){
                mapImg.setImageResource(R.drawable.map_route66);
            }else if(mapno.equals("13")){
                mapImg.setImageResource(R.drawable.map_temple_of_anubis);
            }else if(mapno.equals("14")){
                mapImg.setImageResource(R.drawable.map_volskaya_industries);
            }else if(mapno.equals("15")){
                mapImg.setImageResource(R.drawable.map_watchpointgibraltar);
            }
            // CHECK CHARACTERS
            // 1st Hero
            if(hero1no.equals("0")){
                Btn1.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero1no.equals("1")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero1no.equals("2")){
                Btn1.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero1no.equals("3")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero1no.equals("4")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero1no.equals("5")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero1no.equals("6")){
                Btn1.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero1no.equals("7")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero1no.equals("8")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero1no.equals("9")){
                Btn1.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero1no.equals("10")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero1no.equals("11")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero1no.equals("12")){
                Btn1.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero1no.equals("13")){
                Btn1.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero1no.equals("14")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero1no.equals("15")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero1no.equals("16")){
                Btn1.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero1no.equals("17")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero1no.equals("18")){
                Btn1.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero1no.equals("19")){
                Btn1.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero1no.equals("20")){
                Btn1.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero1no.equals("21")){
                Btn1.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero1no.equals("22")){
                Btn1.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
            // 2nd Hero
            if(hero2no.equals("0")){
                Btn2.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero2no.equals("1")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero2no.equals("2")){
                Btn2.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero2no.equals("3")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero2no.equals("4")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero2no.equals("5")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero2no.equals("6")){
                Btn2.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero2no.equals("7")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero2no.equals("8")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero2no.equals("9")){
                Btn2.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero2no.equals("10")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero2no.equals("11")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero2no.equals("12")){
                Btn2.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero2no.equals("13")){
                Btn2.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero2no.equals("14")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero2no.equals("15")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero2no.equals("16")){
                Btn2.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero2no.equals("17")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero2no.equals("18")){
                Btn2.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero2no.equals("19")){
                Btn2.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero2no.equals("20")){
                Btn2.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero2no.equals("21")){
                Btn2.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero2no.equals("22")){
                Btn2.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
            // 3rd Hero
            if(hero3no.equals("0")){
                Btn3.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero3no.equals("1")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero3no.equals("2")){
                Btn3.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero3no.equals("3")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero3no.equals("4")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero3no.equals("5")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero3no.equals("6")){
                Btn3.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero3no.equals("7")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero3no.equals("8")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero3no.equals("9")){
                Btn3.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero3no.equals("10")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero3no.equals("11")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero3no.equals("12")){
                Btn3.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero3no.equals("13")){
                Btn3.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero3no.equals("14")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero3no.equals("15")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero3no.equals("16")){
                Btn3.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero3no.equals("17")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero3no.equals("18")){
                Btn3.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero3no.equals("19")){
                Btn3.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero3no.equals("20")){
                Btn3.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero3no.equals("21")){
                Btn3.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero3no.equals("22")){
                Btn3.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
            // 4th Hero
            if(hero4no.equals("0")){
                Btn4.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero4no.equals("1")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero4no.equals("2")){
                Btn4.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero4no.equals("3")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero4no.equals("4")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero4no.equals("5")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero4no.equals("6")){
                Btn4.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero4no.equals("7")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero4no.equals("8")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero4no.equals("9")){
                Btn4.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero4no.equals("10")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero4no.equals("11")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero4no.equals("12")){
                Btn4.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero4no.equals("13")){
                Btn4.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero4no.equals("14")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero4no.equals("15")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero4no.equals("16")){
                Btn4.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero4no.equals("17")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero4no.equals("18")){
                Btn4.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero4no.equals("19")){
                Btn4.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero4no.equals("20")){
                Btn4.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero4no.equals("21")){
                Btn4.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero4no.equals("22")){
                Btn4.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
            // 5th Hero
            if(hero5no.equals("0")){
                Btn5.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero5no.equals("1")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero5no.equals("2")){
                Btn5.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero5no.equals("3")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero5no.equals("4")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero5no.equals("5")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero5no.equals("6")){
                Btn5.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero5no.equals("7")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero5no.equals("8")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero5no.equals("9")){
                Btn5.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero5no.equals("10")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero5no.equals("11")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero5no.equals("12")){
                Btn5.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero5no.equals("13")){
                Btn5.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero5no.equals("14")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero5no.equals("15")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero5no.equals("16")){
                Btn5.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero5no.equals("17")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero5no.equals("18")){
                Btn5.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero5no.equals("19")){
                Btn5.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero5no.equals("20")){
                Btn5.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero5no.equals("21")){
                Btn5.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero5no.equals("22")){
                Btn5.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
            // 6th Hero
            if(hero6no.equals("0")){
                Btn6.setBackgroundResource(R.drawable.bar_support_ana);
            }else if(hero6no.equals("1")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_bastion);
            }else if(hero6no.equals("2")){
                Btn6.setBackgroundResource(R.drawable.bar_tank_dva);
            }else if(hero6no.equals("3")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_genji);
            }else if(hero6no.equals("4")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_hanzo);
            }else if(hero6no.equals("5")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_junkrat);
            }else if(hero6no.equals("6")){
                Btn6.setBackgroundResource(R.drawable.bar_support_lucio);
            }else if(hero6no.equals("7")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_mccree);
            }else if(hero6no.equals("8")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_mei);
            }else if(hero6no.equals("9")){
                Btn6.setBackgroundResource(R.drawable.bar_support_mercy);
            }else if(hero6no.equals("10")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_pharah);
            }else if(hero6no.equals("11")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_reaper);
            }else if(hero6no.equals("12")){
                Btn6.setBackgroundResource(R.drawable.bar_tank_reinhardt);
            }else if(hero6no.equals("13")){
                Btn6.setBackgroundResource(R.drawable.bar_tank_roadhog);
            }else if(hero6no.equals("14")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_soldier76);
            }else if(hero6no.equals("15")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_sombra);
            }else if(hero6no.equals("16")){
                Btn6.setBackgroundResource(R.drawable.bar_support_symmetr);
            }else if(hero6no.equals("17")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_torbjorn);
            }else if(hero6no.equals("18")){
                Btn6.setBackgroundResource(R.drawable.bar_offense_tracer);
            }else if(hero6no.equals("19")){
                Btn6.setBackgroundResource(R.drawable.bar_defense_windowmaker);
            }else if(hero6no.equals("20")){
                Btn6.setBackgroundResource(R.drawable.bar_tank_winston);
            }else if(hero6no.equals("21")){
                Btn6.setBackgroundResource(R.drawable.bar_tank_zarya);
            }else if(hero6no.equals("22")){
                Btn6.setBackgroundResource(R.drawable.bar_support_zenyatta);
            }
        }

        mapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt = new Intent(Createbuild.this,Map_Detail.class);
                intent_mapBt.putExtra("info",mapno);
                startActivity(intent_mapBt);
            }
        });

        Btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 1;
                Intent intent_hero1Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero1Bt.putExtra("info",hero1no);
//                intent_hero1Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero1Bt);
            }
        });

        Btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 2;
                Intent intent_hero2Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero2Bt.putExtra("info",hero2no);
//                intent_hero2Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero2Bt);
            }
        });

        Btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 3;
                Intent intent_hero3Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero3Bt.putExtra("info",hero3no);
//                intent_hero3Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero3Bt);
            }
        });

        Btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 4;
                Intent intent_hero4Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero4Bt.putExtra("info",hero4no);
//                intent_hero4Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero4Bt);
            }
        });

        Btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 5;
                Intent intent_hero5Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero5Bt.putExtra("info",hero5no);
//                intent_hero5Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero5Bt);
            }
        });

        Btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                item_selected = 6;
                Intent intent_hero6Bt = new Intent(Createbuild.this, Hero_Details.class);
                intent_hero6Bt.putExtra("info",hero6no);
//                intent_hero6Bt.putExtra("slc",Integer.toString(item_selected));
                startActivity(intent_hero6Bt);
            }
        });
    }
}
