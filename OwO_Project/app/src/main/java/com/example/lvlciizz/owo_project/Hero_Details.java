package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Hero_Details extends AppCompatActivity {

    private FloatingActionButton backBt;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero__details);

        TextView H_Header = findViewById(R.id.txtinfo);
        TextView H_name = findViewById(R.id.hero_name);
        ImageView H_image = findViewById(R.id.hero_image);
        ImageView H_story = findViewById(R.id.H_story);
        ImageView H_ability_1 = findViewById(R.id.H_ability_1);
        ImageView H_ability_2 = findViewById(R.id.H_ability_2);

        backBt = findViewById(R.id.backBt);

        H_Header.setText("      HERO DETAILS");

        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(getIntent() != null)
        {
            String info = getIntent().getStringExtra("info");
            if(info.equals("0")) {

                H_name.setText("ANA : SUPPORT");
                H_image.setImageResource(R.drawable.ana_big);
                H_story.setImageResource(R.drawable.ana_history);
                H_ability_1.setImageResource(R.drawable.ana_ability_1);
                H_ability_2.setImageResource(R.drawable.ana_ability_2);
            }
            else if (info.equals("1")){
                H_name.setText("BASTION : DEFENSE");
                H_image.setImageResource(R.drawable.bastion_big);
                H_story.setImageResource(R.drawable.bastion_history);
                H_ability_1.setImageResource(R.drawable.bastion_ability_1);
                H_ability_2.setImageResource(R.drawable.bastion_ability_2);
            }
            else if (info.equals("2")){
                H_name.setText("DVA : TANK");
                H_image.setImageResource(R.drawable.dva_big);
                H_story.setImageResource(R.drawable.dva_history);
                H_ability_1.setImageResource(R.drawable.dva_ability_1);
                H_ability_2.setImageResource(R.drawable.dva_ability_2);
            }
            else if (info.equals("3")){
                H_name.setText("GENJI : ATTACK");
                H_image.setImageResource(R.drawable.genji_big);
                H_story.setImageResource(R.drawable.genji_history);
                H_ability_1.setImageResource(R.drawable.genji_ability_1);
                H_ability_2.setImageResource(R.drawable.genji_ability_2);
            }
            else if (info.equals("4")){
                H_name.setText("HANZO : DEFENSE");
                H_image.setImageResource(R.drawable.hanzo_big);
                H_story.setImageResource(R.drawable.hanzo_history);
                H_ability_1.setImageResource(R.drawable.hanzo_ability_1);
                H_ability_2.setImageResource(R.drawable.hanzo_ability_2);
            }
            else if (info.equals("5")){
                H_name.setText("JUNKRAT : DEFENSE");
                H_image.setImageResource(R.drawable.junkrat_big);
                H_story.setImageResource(R.drawable.junkrat_history);
                H_ability_1.setImageResource(R.drawable.junkrat_ability_1);
                H_ability_2.setImageResource(R.drawable.junkrat_ability_2);
            }
            else if (info.equals("6")){
                H_name.setText("LUCIO : SUPPORT");
                H_image.setImageResource(R.drawable.lucio_big);
                H_story.setImageResource(R.drawable.lucio_history);
                H_ability_1.setImageResource(R.drawable.lucio_ability_1);
                H_ability_2.setImageResource(R.drawable.lucio_ability_2);
            }
            else if (info.equals("7")){
                H_name.setText("MCCREE : ATTACK");
                H_image.setImageResource(R.drawable.mccree_big);
                H_story.setImageResource(R.drawable.mccree_history);
                H_ability_1.setImageResource(R.drawable.mccree_ability_1);
                H_ability_2.setImageResource(R.drawable.mccree_ability_2);
            }
            else if (info.equals("8")){
                H_name.setText("MEI : DEFENSE");
                H_image.setImageResource(R.drawable.mei_big);
                H_story.setImageResource(R.drawable.mei_history);
                H_ability_1.setImageResource(R.drawable.mei_ability_1);
                H_ability_2.setImageResource(R.drawable.mei_ability_2);
            }
            else if (info.equals("9")){
                H_name.setText("MERCY : SUPPORT");
                H_image.setImageResource(R.drawable.mercy_big);
                H_story.setImageResource(R.drawable.mercy_history);
                H_ability_1.setImageResource(R.drawable.mercy_ability_1);
                H_ability_2.setImageResource(R.drawable.mercy_ability_2);
            }
            else if (info.equals("10")){
                H_name.setText("PHARAH: ATTACK");
                H_image.setImageResource(R.drawable.pharah_big);
                H_story.setImageResource(R.drawable.pharah_history);
                H_ability_1.setImageResource(R.drawable.pharah_ability_1);
                H_ability_2.setImageResource(R.drawable.pharah_ability_2);
            }
            else if (info.equals("11")){
                H_name.setText("REAPER : ATTACK");
                H_image.setImageResource(R.drawable.reaper_big);
                H_story.setImageResource(R.drawable.reaper_history);
                H_ability_1.setImageResource(R.drawable.reaper_ability_1);
                H_ability_2.setImageResource(R.drawable.reaper_ability_2);
            }
            else if (info.equals("12")){
                H_name.setText("REINHARDT : TANK");
                H_image.setImageResource(R.drawable.reinhardt_big);
                H_story.setImageResource(R.drawable.reinhardt_history);
                H_ability_1.setImageResource(R.drawable.reinhardt_ability_1);
                H_ability_2.setImageResource(R.drawable.reinhardt_ability_2);
            }
            else if (info.equals("13")){
                H_name.setText("ROADHOG : DEFENSE");
                H_image.setImageResource(R.drawable.roadhog_big);
                H_story.setImageResource(R.drawable.roadhog_history);
                H_ability_1.setImageResource(R.drawable.roadhog_ability_1);
                H_ability_2.setImageResource(R.drawable.roadhog_ability_2);
            }
            else if (info.equals("14")){
                H_name.setText("SOLDIER76 : ATTACK");
                H_image.setImageResource(R.drawable.soldier76_big);
                H_story.setImageResource(R.drawable.soldier76_history);
                H_ability_1.setImageResource(R.drawable.soldier76_ability_1);
                H_ability_2.setImageResource(R.drawable.soldier76_ability_2);
            }
            else if (info.equals("15")){
                H_name.setText("SOMBRA : ATTACK");
                H_image.setImageResource(R.drawable.sombra_big);
                H_story.setImageResource(R.drawable.sombra_history);
                H_ability_1.setImageResource(R.drawable.sombra_ability_1);
                H_ability_2.setImageResource(R.drawable.sombra_ability_2);
            }
            else if (info.equals("16")){
                H_name.setText("SYMMETRA : SUPPORT");
                H_image.setImageResource(R.drawable.symmetra_big);
                H_story.setImageResource(R.drawable.symmetra_history);
                H_ability_1.setImageResource(R.drawable.symmetra_ability_1);
                H_ability_2.setImageResource(R.drawable.symmetra_ability_2);
            }
            else if (info.equals("17")){
                H_name.setText("TORBJORN : DEFENSE");
                H_image.setImageResource(R.drawable.torbjorn_big);
                H_story.setImageResource(R.drawable.torbjorn_history);
                H_ability_1.setImageResource(R.drawable.torbjorn_ability_1);
                H_ability_2.setImageResource(R.drawable.torbjorn_ability_2);
            }
            else if (info.equals("18")){
                H_name.setText("TRACER : ATTACK");
                H_image.setImageResource(R.drawable.tracer_big);
                H_story.setImageResource(R.drawable.tracer_history);
                H_ability_1.setImageResource(R.drawable.tracer_ability_1);
                H_ability_2.setImageResource(R.drawable.tracer_ability_2);
            }
            else if (info.equals("19")){
                H_name.setText("WIDOWMAKER : DEFENSE");
                H_name.setTextSize(40);
                H_image.setImageResource(R.drawable.widowmaker_big);
                H_story.setImageResource(R.drawable.widowmaker_history);
                H_ability_1.setImageResource(R.drawable.widowmaker_ability_1);
                H_ability_2.setImageResource(R.drawable.widowmaker_ability_2);
            }
            else if (info.equals("20")){
                H_name.setText("WINSTON : TANK");
                H_image.setImageResource(R.drawable.winston_big);
                H_story.setImageResource(R.drawable.winston_history);
                H_ability_1.setImageResource(R.drawable.winston_ability_1);
                H_ability_2.setImageResource(R.drawable.winston_ability_2);
            }
            else if (info.equals("21")){
                H_name.setText("ZARYA : TANK");
                H_image.setImageResource(R.drawable.zarya_big);
                H_story.setImageResource(R.drawable.zarya_history);
                H_ability_1.setImageResource(R.drawable.zarya_ability_1);
                H_ability_2.setImageResource(R.drawable.zarya_ability_2);
            }
            else if (info.equals("22")){
                H_name.setText("ZENYATTA : SUPPORT");
                H_image.setImageResource(R.drawable.zenyatta_big);
                H_story.setImageResource(R.drawable.zenyatta_history);
                H_ability_1.setImageResource(R.drawable.zenyatta_ability_1);
                H_ability_2.setImageResource(R.drawable.zenyatta_ability_2);
            }


        }
    }
}
