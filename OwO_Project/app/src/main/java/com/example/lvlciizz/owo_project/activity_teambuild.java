package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class activity_teambuild extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teambuild);

        Button create = findViewById(R.id.createbuild_button);
        Button build1Bt = findViewById(R.id.button_hanamura);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_create = new Intent(activity_teambuild.this,Createbuild.class);
                startActivity(intent_create);
            }
        });

        build1Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_build1Bt = new Intent(activity_teambuild.this,Build_Detail.class);
                startActivity(intent_build1Bt);
            }
        });
    }

}
