package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HOME extends AppCompatActivity {

    private RecyclerView mBuildList;
    private DatabaseReference mRef;
    private FirebaseRecyclerAdapter<Build_Home, HOME.BuildHomeViewHolder> mBuildAdt;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button infoBt = (Button) findViewById(R.id.infoBt);
        Button buildBt = (Button) findViewById(R.id.buildBt);

        infoBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_infoBt = new Intent(HOME.this, Heroes.class);
                startActivity(intent_infoBt);
            }
        });

        buildBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_buildBt = new Intent(HOME.this, Select_map.class);
                startActivity(intent_buildBt);
            }
        });

        mRef = FirebaseDatabase.getInstance().getReference().child("Build");
        mRef.keepSynced(true);

        DatabaseReference personsRef = FirebaseDatabase.getInstance().getReference().child("Build");
        Query personsQuery = personsRef.orderByKey();

        //Recycler View
        mBuildList = findViewById(R.id.myrecyclerview);
        mBuildList.setHasFixedSize(true);
        mBuildList.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Build_Home>().setQuery(personsQuery, Build_Home.class).build();

        mBuildAdt = new FirebaseRecyclerAdapter<Build_Home, BuildHomeViewHolder>(options) {
            @Override
            protected void onBindViewHolder(BuildHomeViewHolder holder, int position, final Build_Home model) {
                holder.setHero1(getApplicationContext(), model.getHero1());
                holder.setHero2(getApplicationContext(), model.getHero2());
                holder.setHero3(getApplicationContext(), model.getHero3());
                holder.setHero4(getApplicationContext(), model.getHero4());
                holder.setHero5(getApplicationContext(), model.getHero5());
                holder.setHero6(getApplicationContext(), model.getHero6());
                holder.setMapImage(getApplicationContext(), model.getMapimage());

                //setOnClickListener Here
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String hero1 = model.getHero1no().toString();
                        final String hero2 = model.getHero2no().toString();
                        final String hero3 = model.getHero3no().toString();
                        final String hero4 = model.getHero4no().toString();
                        final String hero5 = model.getHero5no().toString();
                        final String hero6 = model.getHero6no().toString();
                        final String mapno = model.getMapno().toString();

                        Intent intent = new Intent(getApplicationContext(), Createbuild.class); // test for intent by intent to information act.
                        intent.putExtra("hero1no", hero1);
                        intent.putExtra("hero2no", hero2);
                        intent.putExtra("hero3no", hero3);
                        intent.putExtra("hero4no", hero4);
                        intent.putExtra("hero5no", hero5);
                        intent.putExtra("hero6no", hero6);
                        intent.putExtra("mapno", mapno);

                        startActivity(intent);
                    }
                });
            }

            @Override
            public BuildHomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.build_list, parent, false);

                return new HOME.BuildHomeViewHolder(view);
            }
        };

        mBuildList.setAdapter(mBuildAdt);
    }

    public static class BuildHomeViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public BuildHomeViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setHero1(Context ctx, String hero1) {
            ImageView post_hero1 = mView.findViewById(R.id.post_hero1);
            Picasso.with(ctx).load(hero1).into(post_hero1);
        }
        public void setHero2(Context ctx, String hero2) {
            ImageView post_hero2 = mView.findViewById(R.id.post_hero2);
            Picasso.with(ctx).load(hero2).into(post_hero2);
        }
        public void setHero3 (Context ctx, String hero3) {
            ImageView post_hero3 = mView.findViewById(R.id.post_hero3);
            Picasso.with(ctx).load(hero3).into(post_hero3);
        }
        public void setHero4(Context ctx, String hero4) {
            ImageView post_hero4 = mView.findViewById(R.id.post_hero4);
            Picasso.with(ctx).load(hero4).into(post_hero4);
        }
        public void setHero5(Context ctx, String hero5) {
            ImageView post_hero5 = mView.findViewById(R.id.post_hero5);
            Picasso.with(ctx).load(hero5).into(post_hero5);
        }
        public void setHero6(Context ctx, String hero6) {
            ImageView post_hero6 = mView.findViewById(R.id.post_hero6);
            Picasso.with(ctx).load(hero6).into(post_hero6);
        }
        public void setMapImage(Context ctx, String mapimage) {
            ImageView post_map = mView.findViewById(R.id.post_map);
            Picasso.with(ctx).load(mapimage).into(post_map);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBuildAdt.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBuildAdt.stopListening();
    }
}
