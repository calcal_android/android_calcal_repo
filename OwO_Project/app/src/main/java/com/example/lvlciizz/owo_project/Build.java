package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Build extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build);

        Button assaultBt = (Button) findViewById(R.id.button_assault);
        Button assault_escortBt = (Button) findViewById(R.id.button_assault_and_escort);
        Button controlBt = (Button) findViewById(R.id.button_control);
        Button escortBt = (Button) findViewById(R.id.button_escort);

        assaultBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_assaultBt = new Intent(Build.this, Assault_map.class);
                startActivity(intent_assaultBt);
            }
        });

        assault_escortBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_assault_escortBt = new Intent(Build.this, Assault_and_escort.class);
                startActivity(intent_assault_escortBt);
            }
        });

        controlBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_controlBt = new Intent(Build.this, Control_map.class);
                startActivity(intent_controlBt);
            }
        });

        escortBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_escortBt = new Intent(Build.this, Escort_map.class);
                startActivity(intent_escortBt);
            }
        });
    }
}
