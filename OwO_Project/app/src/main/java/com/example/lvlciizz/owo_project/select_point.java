package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class select_point extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_point);

        Button point1Bt = (Button) findViewById(R.id.button_point1);
        Button point2Bt = (Button) findViewById(R.id.button_point2);
        Button point3Bt = (Button) findViewById(R.id.button_point3);

        point1Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_point1 = new Intent(select_point.this, activity_teambuild.class);
                startActivity(intent_point1);
            }
        });

        point2Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_point2 = new Intent(select_point.this, activity_teambuild.class);
                startActivity(intent_point2);
            }
        });

        point3Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_point3 = new Intent(select_point.this, activity_teambuild.class);
                startActivity(intent_point3);
            }
        });
    }
}
