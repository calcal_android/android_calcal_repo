package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class side extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side);

        Button atkBt = (Button) findViewById(R.id.button_attack);
        Button defBt = (Button) findViewById(R.id.button_defense);

        atkBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_atkBt = new Intent(side.this, activity_teambuild.class);
                startActivity(intent_atkBt);
            }
        });

        defBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_defBt = new Intent(side.this, activity_teambuild.class);
                startActivity(intent_defBt);
            }
        });
    }
}
