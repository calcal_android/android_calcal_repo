package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Information extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        Button heroslBt = (Button) findViewById(R.id.heroslBt);
        Button mapslBt = (Button) findViewById(R.id.mapslBt);

        heroslBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_heroslBt = new Intent(Information.this,Heroes.class);
                startActivity(intent_heroslBt);
            }
        });

        mapslBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapslBt = new Intent(Information.this, Select_map.class);
                startActivity(intent_mapslBt);
            }
        });
    }
}
