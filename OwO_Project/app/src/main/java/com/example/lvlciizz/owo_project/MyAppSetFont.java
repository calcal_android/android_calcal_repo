package com.example.lvlciizz.owo_project;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by lvlciizz on 22-Jan-18.
 */

public class MyAppSetFont extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initFont();
    }

    private void initFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/font_overwatch.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
