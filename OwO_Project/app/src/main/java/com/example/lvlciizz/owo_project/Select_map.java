package com.example.lvlciizz.owo_project;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Select_map extends AppCompatActivity {

    private FloatingActionButton homeBt;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_map);

        Button mapBtn01 = (Button) findViewById(R.id.doradoBtn);
        Button mapBtn02 = (Button) findViewById(R.id.eichenBtn);
        Button mapBtn03 = (Button) findViewById(R.id.hanaBtn);
        Button mapBtn04 = (Button) findViewById(R.id.hollyBtn);
        Button mapBtn05 = (Button) findViewById(R.id.horiBtn);
        Button mapBtn06 = (Button) findViewById(R.id.iliosBtn);
        Button mapBtn07 = (Button) findViewById(R.id.junkBtn);
        Button mapBtn08 = (Button) findViewById(R.id.kingsBtn);
        Button mapBtn09 = (Button) findViewById(R.id.lijiangBtn);
        Button mapBtn10 = (Button) findViewById(R.id.napalBtn);
        Button mapBtn11 = (Button) findViewById(R.id.numbaniBtn);
        Button mapBtn12 = (Button) findViewById(R.id.oasisBtn);
        Button mapBtn13 = (Button) findViewById(R.id.routeBtn);
        Button mapBtn14 = (Button) findViewById(R.id.templeBtn);
        Button mapBtn15 = (Button) findViewById(R.id.volskaBtn);
        Button mapBtn16 = (Button) findViewById(R.id.gibrotarBtn);

        homeBt = findViewById(R.id.homeBt);
        homeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(Select_map.this, HOME.class);
                startActivity(home_intent);
            }
        });


        mapBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt01 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt01.putExtra("info","0");
                startActivity(intent_mapBt01);
            }
        });

        mapBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt02 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt02.putExtra("info","1");
                startActivity(intent_mapBt02);
            }
        });

        mapBtn03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt03 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt03.putExtra("info","2");
                startActivity(intent_mapBt03);
            }
        });

        mapBtn04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt04 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt04.putExtra("info","3");
                startActivity(intent_mapBt04);
            }
        });

        mapBtn05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt05 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt05.putExtra("info","4");
                startActivity(intent_mapBt05);
            }
        });
        mapBtn06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt06 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt06.putExtra("info","5");
                startActivity(intent_mapBt06);
            }
        });
        mapBtn07.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt07 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt07.putExtra("info","6");
                startActivity(intent_mapBt07);
            }
        });
        mapBtn08.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt08 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt08.putExtra("info","7");
                startActivity(intent_mapBt08);
            }
        });
        mapBtn09.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt09 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt09.putExtra("info","8");
                startActivity(intent_mapBt09);
            }
        });
        mapBtn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt10 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt10.putExtra("info","9");
                startActivity(intent_mapBt10);
            }
        });
        mapBtn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt11 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt11.putExtra("info","10");
                startActivity(intent_mapBt11);
            }
        });
        mapBtn12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt12 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt12.putExtra("info","11");
                startActivity(intent_mapBt12);
            }
        });
        mapBtn13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt13 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt13.putExtra("info","12");
                startActivity(intent_mapBt13);
            }
        });
        mapBtn14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt14 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt14.putExtra("info","13");
                startActivity(intent_mapBt14);
            }
        });
        mapBtn15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt15 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt15.putExtra("info","14");
                startActivity(intent_mapBt15);
            }
        });
        mapBtn16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_mapBt16 = new Intent(Select_map.this, Map_Detail.class);
                intent_mapBt16.putExtra("info","15");
                startActivity(intent_mapBt16);
            }
        });
    }
}
