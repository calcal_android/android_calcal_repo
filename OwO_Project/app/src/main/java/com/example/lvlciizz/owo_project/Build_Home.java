package com.example.lvlciizz.owo_project;

/**
 * Created by lvlciizz on 06-Mar-18.
 */

public class Build_Home {

    private String hero1, hero2, hero3, hero4, hero5, hero6;
    private String mapimage;

    private Long hero1no, hero2no, hero3no, hero4no, hero5no, hero6no;
    private Long mapno;

    public Build_Home() {}

    public Build_Home(String hero1, String hero2, String hero3, String hero4, String hero5, String hero6, String mapimage, Long hero1no, Long hero2no, Long hero3no, Long hero4no, Long hero5no, Long hero6no, Long mapno) {
        this.hero1 = hero1;
        this.hero2 = hero2;
        this.hero3 = hero3;
        this.hero4 = hero4;
        this.hero5 = hero5;
        this.hero6 = hero6;
        this.mapimage = mapimage;
        this.hero1no = hero1no;
        this.hero2no = hero2no;
        this.hero3no = hero3no;
        this.hero4no = hero4no;
        this.hero5no = hero5no;
        this.hero6no = hero6no;
        this.mapno = mapno;
    }

    public String getHero1() {
        return hero1;
    }

    public void setHero1(String hero1) {
        this.hero1 = hero1;
    }

    public String getHero2() {
        return hero2;
    }

    public void setHero2(String hero2) {
        this.hero2 = hero2;
    }

    public String getHero3() {
        return hero3;
    }

    public void setHero3(String hero3) {
        this.hero3 = hero3;
    }

    public String getHero4() {
        return hero4;
    }

    public void setHero4(String hero4) {
        this.hero4 = hero4;
    }

    public String getHero5() {
        return hero5;
    }

    public void setHero5(String hero5) {
        this.hero5 = hero5;
    }

    public String getHero6() {
        return hero6;
    }

    public void setHero6(String hero6) {
        this.hero6 = hero6;
    }

    public String getMapimage() {
        return mapimage;
    }

    public void setMapimage(String mapimage) {
        this.mapimage = mapimage;
    }

    public Long getHero1no() {
        return hero1no;
    }

    public void setHero1no(Long hero1no) {
        this.hero1no = hero1no;
    }

    public Long getHero2no() {
        return hero2no;
    }

    public void setHero2no(Long hero2no) {
        this.hero2no = hero2no;
    }

    public Long getHero3no() {
        return hero3no;
    }

    public void setHero3no(Long hero3no) {
        this.hero3no = hero3no;
    }

    public Long getHero4no() {
        return hero4no;
    }

    public void setHero4no(Long hero4no) {
        this.hero4no = hero4no;
    }

    public Long getHero5no() {
        return hero5no;
    }

    public void setHero5no(Long hero5no) {
        this.hero5no = hero5no;
    }

    public Long getHero6no() {
        return hero6no;
    }

    public void setHero6no(Long hero6no) {
        this.hero6no = hero6no;
    }

    public Long getMapno() {
        return mapno;
    }

    public void setMapno(Long mapno) {
        this.mapno = mapno;
    }
}
